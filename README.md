## dsPIC-EL-GM

A small PIC Elmer sort of board for the dsPIC33EV256GM102.  Board will
accept Arduino shields, but has buttons and LCD on main board as these
always want to be on the "top" shield.  Also a few LEDs for good
measure.

**Features:**
* dsPIC33EV256GM102
  * 70 MIPS
  * 256K flash
  * 16K RAM
  * 5 timers
  * CAN
  * 2 UART
  * 2 SPI (15MHz, 25MHz if PPS not used)
  * 1 I2C
  * 3 x 2-channel PWM
  * 3 op amp
  * 10/12 bit A/D
  * Peripheral pin select
* Accepts Arduino shields
* Duplicate connector for Arduino pins 8-13 to allow standard perfboard to be used like a shield
* Standard 80mm 16x2 LCD
* Four pushbuttons
* Theee LEDs
* LCD, buttons and LEDs can be disabled with jumpers
* Jumpers to allow PIC24FVxxKyy02 programming

**Supported Parts:**
* dsPIC33EV256GM102
* dsPIC33EV256GM002
* dsPIC33EV128GM102
* dsPIC33EV128GM002
* dsPIC33EV64GM102
* dsPIC33EV64GM002
* dsPIC33EV32GM102
* dsPIC33EV32GM002
* PIC24FV32KA302
* PIC24FV16KA302
* PIC24FV16KM202
* PIC24FV16KM102
* PIC24FV08KM202
* PIC24FV08KM102



---

![Prototype](https://gitlab.com/33E-simple/dsPIC-EL-GM/raw/master/images/dsPIC-EL-built.jpg)

**Prototype board**

---

![Schematic](https://gitlab.com/33E-simple/dsPIC-EL-GM/raw/master/dsPIC-EL-GM.png)

**Schematic**

----

![PCB Layout](https://gitlab.com/33E-simple/dsPIC-EL-GM/raw/master/dsPIC-EL-GM1-A-RB.png)

**PCB Layout**

----

